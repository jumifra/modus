function buildCarousel(visibleSlides) {
    $('.homeWhychoose--clients--slider').cycle({
        fx: 'carousel',
        speed: 600,
        slides: '.homeWhychoose--clients--slider--int',
        carouselVisible: visibleSlides,
        carouselFluid: true
    });
    $('.homeWhychoose--clients--slider cycle-slideshow .homeWhychoose--clients--slider--int').css('opacity','1');
}
function buildSlideshow() {
    $('.home-slideshow').cycle({
        fx: 'fade',
        slides: 'img',
        timeout: 12000
    });
}

function initCycle() {
    var width = $(document).width();
    var visibleSlides = 5;
    
    if ( width < 400 ) {visibleSlides = 5}
    else if(width < 700) {visibleSlides = 3}
    else {visibleSlides = 5};

    buildSlideshow();
    buildCarousel(visibleSlides);
}

function reinit_cycle() {
    var width = $(window).width();
    
    
    var destroyCarousel = function() { // create a function
        $('.homeWhychoose--clients--slider').cycle('destroy');
    }

    if (width < 400) {
        destroyCarousel(); // call the function
        reinitCycle(6);
    } else if (width > 400 && width < 700) {
        destroyCarousel();
        reinitCycle(3);
    } else {
        destroyCarousel();
        reinitCycle(6);
    }
}

function reinitCycle(visibleSlides) {
    buildCarousel(visibleSlides);
}
var reinitTimer;
$(window).resize(function() {
    clearTimeout(reinitTimer);
    reinitTimer = setTimeout(reinit_cycle, 100);
});

$(document).ready(function(){
    initCycle();    
});