<footer class="footer">
		<div class="footer-lineas"></div>
		<div class="footerTop">
			<div class="container">
				<div class="footerTop--info footerTop--col1">
					<div class="footerTop--info--logo">
						<a href="#"><h1>MODUS<span>versus</span></h1></a>
					</div>
					<p class="footerTop--info--txt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec .</p>
					<ul class="footerTop--info--contact">
						<li class="footerTop--info--contact--int">
							<p class="footerTop--info--contact--int--name">Phone:</p>
							<span class="footerTop--info--contact--int--info">182 2569 5896</span>
						</li>
						<li class="footerTop--info--contact--int">
							<p class="footerTop--info--contact--int--name">e-mail:</p>
							<span class="footerTop--info--contact--int--info">info@modu.versus</span>
						</li>
					</ul>
				</div>
				<div class="footerTop--list footerTop--col2">
					<h3 class="footerTop--list--title">Company</h3>
					<ul class="footerTop--list--int">
						<li class="footerTop--list--int--info">
							<a href="#" class="footerTop--list--int--info--txt"><i class="footerTop--list--int--info--txt--ico fas fa-chevron-right"></i>About</a>
						</li>
						<li class="footerTop--list--int--info">
							<a href="#" class="footerTop--list--int--info--txt"><i class="footerTop--list--int--info--txt--ico fas fa-chevron-right"></i>FAQ</a>
						</li>
						<li class="footerTop--list--int--info">
							<a href="#" class="footerTop--list--int--info--txt"><i class="footerTop--list--int--info--txt--ico fas fa-chevron-right"></i>Contact</a>
						</li>
						<li class="footerTop--list--int--info">
							<a href="#" class="footerTop--list--int--info--txt"><i class="footerTop--list--int--info--txt--ico fas fa-chevron-right"></i>Terms</a>
						</li>
						<li class="footerTop--list--int--info">
							<a href="#" class="footerTop--list--int--info--txt"><i class="footerTop--list--int--info--txt--ico fas fa-chevron-right"></i>Privacy</a>
						</li>
						<li class="footerTop--list--int--info">
							<a href="#" class="footerTop--list--int--info--txt"><i class="footerTop--list--int--info--txt--ico fas fa-chevron-right"></i>Testimonials</a>
						</li>
					</ul>
				</div>
				<div class="footerTop--list footerTop--col3">
					<h3 class="footerTop--list--title">Community</h3>
					<ul class="footerTop--list--int">
						<li class="footerTop--list--int--info">
							<a href="#" class="footerTop--list--int--info--txt"><i class="footerTop--list--int--info--txt--ico fas fa-chevron-right"></i>Blog</a>
						</li>
						<li class="footerTop--list--int--info">
							<a href="#" class="footerTop--list--int--info--txt"><i class="footerTop--list--int--info--txt--ico fas fa-chevron-right"></i>Forum</a>
						</li>
						<li class="footerTop--list--int--info">
							<a href="#" class="footerTop--list--int--info--txt"><i class="footerTop--list--int--info--txt--ico fas fa-chevron-right"></i>Support</a>
						</li>
						<li class="footerTop--list--int--info">
							<a href="#" class="footerTop--list--int--info--txt"><i class="footerTop--list--int--info--txt--ico fas fa-chevron-right"></i>Newsletter</a>
						</li>
					</ul>
				</div>
				<div class="footerTop--blog footerTop--col4">
					<h3 class="footerTop--blog--title">from the <span class="footerTop--blog--title--bold">BLOG</span></h3>
					<ul class="footerTop--blog--cont">
						<li class="footerTop--blog--cont--int">
							<div class="footerTop--blog--cont--int--img">
								<img src="<?php echo get_template_directory_uri(); ?>/img/foto1.jpg" class="footerTop--blog--cont--int--img--image"/>
							</div>
							<div class="footerTop--blog--cont--int--info">
								<p class="footerTop--blog--cont--int--info--desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta nostrum, ipsam accusantium.</p>
								<p class="footerTop--blog--cont--int--info--date">26 May, 2013</p>
							</div>
						</li>
						<li class="footerTop--blog--cont--int">
							<div class="footerTop--blog--cont--int--img">
								<img src="<?php echo get_template_directory_uri(); ?>/img/foto1.jpg" class="footerTop--blog--cont--int--img--image"/>
							</div>
							<div class="footerTop--blog--cont--int--info">
								<p class="footerTop--blog--cont--int--info--desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta nostrum, ipsam accusantium.</p>
								<p class="footerTop--blog--cont--int--info--date">26 May, 2013</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="footerDown">
			<div class="container">
				<div class="footerDown--copy">
					<p class="footerDown--copy--txt">2013  ModusVersus</p>
				</div>
				<div class="footerDown--redes">
					<ul class="footerDown--redes--int">
						<li class="footerDown--redes--int--social"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
						<li class="footerDown--redes--int--social"><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
						<li class="footerDown--redes--int--social"><a href="#"><i class="fab fa-tumblr"></i></a></li>
						<li class="footerDown--redes--int--social"><a href="#"><i class="fas fa-rss"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>

<?php wp_footer(); ?>
</body>
</html>