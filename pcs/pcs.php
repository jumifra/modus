<?php

// HEADER

defined('ABSPATH') or die("No script kiddies please!");

// HEADER END

define('TEMPLATE_URL', get_stylesheet_directory_uri());
define('URL', get_bloginfo('url'));

require 'PicolCodeSuite.class.php';
require 'WordPress.class.php';

PicolCodeSuite::_start();

PicolCodeSuite::themeSetup(dirname(__FILE__), 'picol_theme_setup');

/*
 * HELPERS
 */

if(!function_exists('_g')){

    function _g( $key, $default = FALSE ) {
        return isset($_GET[$key]) ? $_GET[$key] : $default;
    }

    function _p( $key, $default = FALSE ) {
        return isset($_POST[$key]) ? $_POST[$key] : $default;
    }

    function _r( $key, $default = FALSE ) {
        return isset($_REQUEST[$key]) ? $_REQUEST[$key] : $default;
    }

    function doing_post_request() {
        return stristr( $_SERVER['REQUEST_METHOD'], 'post');
    }

}

