<section class="homeWhychoose">
	<div class="container">
		<!-- <div class="homeWhychoose--info">
		   
		<?php 
		$wp_query = new WP_Query( array(
	      'post_type' => 'a_post_type',
	      'posts_per_page' => 5,
	      'post_status' => 'publish',
	      'tax_query' => array(
	        array(
	          'taxonomy' => 'a_categoria', // Nombre de la taxonomía creada
	          'field' => 'slug', // Como pasaremos el parámetro, en este caso como el slug
	          'terms' => 'testimonios', // Slug de la taxonomía
	        )
	      )
	    ));
	     // Loop WordPress
	     while ($wp_query->have_posts()) : $wp_query->the_post();
	       the_content();
	     endwhile;	
		?>


		</div> -->
		<div class="homeWhychoose--info">
			<?php if( have_rows('about_list_choose') ): 
				while( have_rows('about_list_choose') ): the_row(); 
					// vars
					$textabout = get_sub_field('about_title_list_choose');
					$items = get_sub_field('about_list_choose_items');
					?>
					<h3 class="homeWhychoose--info--title"><?php echo $textabout; ?></h3>
					<?php if( have_rows('about_list_choose_items') ): ?>
						<ul class="homeWhychoose--info--content">
							<?php while( have_rows('about_list_choose_items') ): the_row(); 
								// vars
								$content = get_sub_field('about_list_item');
								?>
								<li class="homeWhychoose--info--content--txt"><i class="fas fa-arrow-right"></i><?php echo $content; ?></li>
							<?php endwhile; ?>
						</ul>
					<?php endif; ?>
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
		<div class="homeWhychoose--graphics">
			<?php if( have_rows('skills_grupo') ): 
					while( have_rows('skills_grupo') ): the_row(); 
						
						// vars
						$skilltext = get_sub_field('skills_texto');
						$skillitem = get_sub_field('grupo_skills');
						
						?>
						<p class="homeWhychoose--graphics--txt"><?php echo $skilltext; ?></p>
						
							
						<?php if( have_rows('grupo_skills') ): ?>

							<div class="homeWhychoose--graphics--barras">

									<?php while( have_rows('grupo_skills') ): the_row(); 

										// vars
										$skillname = get_sub_field('skill_name');
										$skillpercen = get_sub_field('skill_percentage');

										?>
										<div class="homeWhychoose--graphics--barras--int">
											<div class="homeWhychoose--graphics--barras--int--circle <?php echo $skillname; ?>">
												<style type="text/css">
													.<?php echo $skillname; ?>{
														transform: rotate(calc(<?php echo $skillpercen; ?>deg / 100 * 180));
													}
													.<?php echo $skillname; ?>:before{
														content: "<?php echo $skillpercen; ?>";
														transform: rotate(calc(-<?php echo $skillpercen; ?>deg / 100 * 180));
													}
													.<?php echo $skillname; ?>:after{
														content: "<?php echo $skillname; ?>";
														transform: rotate(calc(-<?php echo $skillpercen; ?>deg / 100 * 180)) scale(1.1);
													}
												</style>
											</div>
										</div>

									<?php endwhile; ?>

								
							</div>

						<?php endif; ?>
							

					<?php endwhile; ?>
					
				<?php endif; ?>
			
		</div>


		<!-- <div class="homeWhychoose--graphics">
			<p class="homeWhychoose--graphics--txt">Curabitur quis nisl in leo euismod venenatis eu in diam. Etiam auctor diam pellentesque lectus vehicula mattis. Nulla ac massa at dolor condimentum eleifend vitae vitae urna.</p>
			
			<div class="homeWhychoose--graphics--barras">
				<div class="homeWhychoose--graphics--barras--int">
					<div class="homeWhychoose--graphics--barras--int--circle suspendisse">
						<style type="text/css">
							.suspendisse{
								transform: rotate(90deg);
							}
							.suspendisse:before{
								content: "50";
								transform: rotate(-90deg);
							}
							.suspendisse:after{
								content: "suspendisse";
								transform: rotate(-90deg) scale(1.1);
							}
						</style>
					</div>
				</div>
				<div class="homeWhychoose--graphics--barras--int">
					<div class="homeWhychoose--graphics--barras--int--circle maecenas">
						<style type="text/css">
							.maecenas{
								transform: rotate(126deg);
							}
							.maecenas:before{
								content: "70";
								transform: rotate(-126deg);
							}
							.maecenas:after{
								content: "maecenas";
								transform: rotate(-126deg) scale(1.1);
							}
						</style>
					</div>
				</div>
				<div class="homeWhychoose--graphics--barras--int">
					<div class="homeWhychoose--graphics--barras--int--circle aliquam">
						<style type="text/css">
							.aliquam{
								transform: rotate(calc(80deg / 100 * 180));
							}
							.aliquam:before{
								content: "80";
								transform: rotate(calc(-80deg / 100 * 180));
							}
							.aliquam:after{
								content: "aliquam";
								transform: rotate(calc(-80deg / 100 * 180));
							}
						</style>
					</div>
				</div>
				<div class="homeWhychoose--graphics--barras--int">
					<div class="homeWhychoose--graphics--barras--int--circle habitasse">
						<style type="text/css">
							.habitasse{
								transform: rotate(180deg);
							}
							.habitasse:before{
								content: "100";
								transform: rotate(-180deg);
							}
							.habitasse:after{
								content: "habitasse";
								transform: rotate(-180deg) scale(1.1);
							}
						</style>
					</div>
				</div>
			</div>
		</div> -->
		
		
		<div class="homeWhychoose--testimonios">
			<?php if( have_rows('testimonios') ): 

				while( have_rows('testimonios') ): the_row(); 
					
					// vars
					$testi_titulo = get_sub_field('titulo_testimonios');
					$testi_publicacion = get_sub_field('publicacion_testimonio');
					
					?>
					<h3 class="homeWhychoose--info--title"><?php echo $testi_titulo; ?></h3>
					
						
					<?php if( have_rows('publicacion_testimonio') ): ?>

						<div class="homeWhychoose--testimonios--slider cycle-slideshow"
							data-cycle-fx="fade"
							data-cycle-timeout="4000"
							data-cycle-slides="> div"
						>

							<?php while( have_rows('publicacion_testimonio') ): the_row(); 

								// vars
								$testi_opinion_autor = get_sub_field('opinion_del_autor');
								$testi_nombre_autor = get_sub_field('nombre_del_autor');
								?>

								<div class="homeWhychoose--testimonios--slider--content">
									<p class="homeWhychoose--testimonios--slider--content--txt"><?php echo $testi_opinion_autor; ?></p>
									<h4 class="homeWhychoose--testimonios--slider--content--title"><?php echo $testi_nombre_autor; ?></h4>
								</div>

							<?php endwhile; ?>

						</div>

					<?php endif; ?>


				<?php endwhile; ?>
				
			<?php endif; ?>
		</div>
		<!-- <div class="homeWhychoose--testimonios">
			<h4 class="homeWhychoose--testimonios--title">What Client's Say?</h4>
			<div class="homeWhychoose--testimonios--slider cycle-slideshow"
				data-cycle-fx="fade"
				data-cycle-timeout="4000"
				data-cycle-slides="> div"
			>
				<div class="homeWhychoose--testimonios--slider--content">
					<p class="homeWhychoose--testimonios--slider--content--txt">Etiam auctor diam pellentesque lectus vehicula mattis. Nulla ac massa at dolorCurabitur quis nisl in leo euismod venenatis eu in diam. condimentum</p>
					<h4 class="homeWhychoose--testimonios--slider--content--title">Jhon Doe</h4>
				</div>
				<div class="homeWhychoose--testimonios--slider--content">
					<p class="homeWhychoose--testimonios--slider--content--txt">Curabitur quis nisl in leo euismod venenatis eu in diam. Etiam auctor diam pellentesque lectus vehicula mattis. Nulla ac massa at dolor condimentum</p>
					<h4 class="homeWhychoose--testimonios--slider--content--title">Jhon Doe</h4>
				</div>
				<div class="homeWhychoose--testimonios--slider--content">
					<p class="homeWhychoose--testimonios--slider--content--txt">enenatis eu in diam. Etiam auctor diam pellentesque lectus vehicula mattis. Nulla aCurabitur quis nisl in leo euismod vc massa at dolor condimentum</p>
					<h4 class="homeWhychoose--testimonios--slider--content--title">Jhon Doe</h4>
				</div>
			</div>
		</div> -->
	</div>
</section>