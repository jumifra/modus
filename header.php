<!DOCTYPE html>
<html <?php language_attributes();?>>
<head>
	<meta charset="UTF-8">
	<title>Modus Versus</title>
	<?php wp_head(); ?>
	<style>
		.slideshow { margin: auto }
		.slideshow img { width: 100px; height: 100px; padding: 2px; }
		div.responsive img { width: auto; height: auto }
		.cycle-pager { position: static; margin-top: 5px }
		div.vertical { width: 100px }
	</style>
</head>
<body>
	<header class="header">
		<div class="container headerVertical">
			<div class="headerVerticalalign">
				<div class="headerLogo logo">
					<a href="#"><h1>MODUS<span>versus</span></h1></a>
				</div>
				<div class="headerMenu">
					<?php 
						if(has_nav_menu('main_menu')):					
						wp_nav_menu(array(
							'theme_location' => 'main_menu',
							'container' => 'nav',
							'container_class' => 'menu menu-main'
						));
						else:?>
						<nav class="menu">
							<ul>
								<?php wp_list_pages('title_li'); ?>
							</ul>
						</nav>
						<?php endif; ?> 
					<div class="headerMenu--search">
						<div class="headerMenu--search--ico">
							<i class="fas fa-search"></i>
							<div class="headerMenu--search--ico--int">
								<form class="headerMenu--search--ico--int--search" id="searchform" method="get" action="<?php echo home_url('/'); ?>">
									<label for="" class="headerMenu--search--ico--int--search--form">
										<input type="text" class="headerMenu--search--ico--int--search--form--field" name="s" placeholder="Search the site" value="<?php the_search_query(); ?>">
								    	<input type="submit" value="Search" class="headerMenu--search--ico--int--search--form--submit">
									</label>
								    
								</form>
							</div>
						</div>
					</div>

						
				</div>
			</div>
		</div>

		<div class="header-lineas"></div>
		<!-- <img src="<?php echo get_template_directory_uri(); ?>/img/lineas-fondo.svg" class="header-lineas"/> -->
	</header>