<?php

function custom_why_choose() {
  $labels = array(
    'name' => _x( 'Why Choose', 'Why Choose General Name', 'mawt' ),
    'singular_name' => _x( 'Why Choose', 'Why Choose Singular Name', 'mawt' ),
    'menu_name' => __( 'Why Choose', 'mawt' ),
    'name_admin_bar' => __( 'Why Choose', 'mawt' ),
    'archives' => __( 'Item Archives', 'mawt' ),
    'attributes' => __( 'Item Attributes', 'mawt' ),
    'parent_item_colon' => __( 'Parent Item:', 'mawt' ),
    'all_items' => __( 'All Items', 'mawt' ),
    'add_new_item' => __( 'Add New Item', 'mawt' ),
    'add_new' => __( 'Add New', 'mawt' ),
    'new_item' => __( 'New Item', 'mawt' ),
    'edit_item' => __( 'Edit Item', 'mawt' ),
    'update_item' => __( 'Update Item', 'mawt' ),
    'view_item' => __( 'View Item', 'mawt' ),
    'view_items' => __( 'View Items', 'mawt' ),
    'search_items' => __( 'Search Item', 'mawt' ),
    'not_found' => __( 'Not found', 'mawt' ),
    'not_found_in_trash' => __( 'Not found in Trash', 'mawt' ),
    'featured_image' => __( 'Featured Image', 'mawt' ),
    'set_featured_image' => __( 'Set featured image', 'mawt' ),
    'remove_featured_image' => __( 'Remove featured image', 'mawt' ),
    'use_featured_image' => __( 'Use as featured image', 'mawt' ),
    'insert_into_item' => __( 'Insert into item', 'mawt' ),
    'uploaded_to_this_item' => __( 'Uploaded to this item', 'mawt' ),
    'items_list' => __( 'Items list', 'mawt' ),
    'items_list_navigation' => __( 'Items list navigation', 'mawt' ),
    'filter_items_list' => __( 'Filter items list', 'mawt' ),
  );

  $args = array(
    'label' => __( 'Why Choose', 'mawt' ),
    'description' => __( 'Why Choose Description', 'mawt' ),
    'labels' => $labels,
    // las taxonomías que soportará
    //'taxonomies' => array( 'category', 'post_tag' ),
    // Todo lo que soporta este post type
        'supports' => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields' ),
        //hierarchical true se comporta como página, false como entrada
        'hierarchical' => false,
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'menu_position' => 5,
    //El icono que tendrá https://developer.wordpress.org/resource/dashicons
    'menu_icon' => 'dashicons-welcome-view-site',
    'show_in_admin_bar' => true,
    'show_in_nav_menus' => true,
    'can_export' => true,
    'has_archive' => true,
    'exclude_from_search' => false,
    'publicly_queryable' => true,
    'capability_type' => 'page',
  );

  register_post_type( 'a_post_type', $args );
}

add_action( 'init', 'custom_why_choose', 0 );

function custom_categoria() {
  $labels = array(
    'name' => _x( 'Categorias', 'Categoria General Name', 'mawt' ),
    'singular_name' => _x( 'Categoria', 'Taxonomy Singular Name', 'mawt' ),
    'menu_name' => __( 'Categoria', 'mawt' ),
    'all_items' => __( 'All Items', 'mawt' ),
    'parent_item' => __( 'Parent Item', 'mawt' ),
    'parent_item_colon' => __( 'Parent Item:', 'mawt' ),
    'new_item_name' => __( 'New Item Name', 'mawt' ),
    'add_new_item' => __( 'Add New Item', 'mawt' ),
    'edit_item' => __( 'Edit Item', 'mawt' ),
    'update_item' => __( 'Update Item', 'mawt' ),
    'view_item' => __( 'View Item', 'mawt' ),
    'separate_items_with_commas' => __( 'Separate items with commas', 'mawt' ),
    'add_or_remove_items' => __( 'Add or remove items', 'mawt' ),
    'choose_from_most_used' => __( 'Choose from the most used', 'mawt' ),
    'popular_items' => __( 'Popular Items', 'mawt' ),
    'search_items' => __( 'Search Items', 'mawt' ),
    'not_found' => __( 'Not Found', 'mawt' ),
    'no_terms' => __( 'No items', 'mawt' ),
    'items_list' => __( 'Items list', 'mawt' ),
    'items_list_navigation' => __( 'Items list navigation', 'mawt' ),
  );

  $args = array(
    'labels' => $labels,
    //hierarchical true se comporta como categoría, false como etiqueta
    'hierarchical' => true,
    'public' => true,
    'show_ui' => true,
    'show_admin_column' => true,
    'show_in_nav_menus' => true,
    'show_tagcloud' => true,
  );
  register_taxonomy( 'a_categoria', array( 'a_post_type' ), $args );
}

add_action( 'init', 'custom_categoria', 0 );














function custom_happy_clients() {
  $labels = array(
    'name' => _x( 'Happy Clients', 'Happy Clients General Name', 'mawt' ),
    'singular_name' => _x( 'Happy Clients', 'Happy Clients Singular Name', 'mawt' ),
    'menu_name' => __( 'Happy Clients', 'mawt' ),
    'name_admin_bar' => __( 'Happy Clients', 'mawt' ),
    'archives' => __( 'Item Archives', 'mawt' ),
    'attributes' => __( 'Item Attributes', 'mawt' ),
    'parent_item_colon' => __( 'Parent Item:', 'mawt' ),
    'all_items' => __( 'All Items', 'mawt' ),
    'add_new_item' => __( 'Add New Item', 'mawt' ),
    'add_new' => __( 'Add New', 'mawt' ),
    'new_item' => __( 'New Item', 'mawt' ),
    'edit_item' => __( 'Edit Item', 'mawt' ),
    'update_item' => __( 'Update Item', 'mawt' ),
    'view_item' => __( 'View Item', 'mawt' ),
    'view_items' => __( 'View Items', 'mawt' ),
    'search_items' => __( 'Search Item', 'mawt' ),
    'not_found' => __( 'Not found', 'mawt' ),
    'not_found_in_trash' => __( 'Not found in Trash', 'mawt' ),
    'featured_image' => __( 'Featured Image', 'mawt' ),
    'set_featured_image' => __( 'Set featured image', 'mawt' ),
    'remove_featured_image' => __( 'Remove featured image', 'mawt' ),
    'use_featured_image' => __( 'Use as featured image', 'mawt' ),
    'insert_into_item' => __( 'Insert into item', 'mawt' ),
    'uploaded_to_this_item' => __( 'Uploaded to this item', 'mawt' ),
    'items_list' => __( 'Items list', 'mawt' ),
    'items_list_navigation' => __( 'Items list navigation', 'mawt' ),
    'filter_items_list' => __( 'Filter items list', 'mawt' ),
  );

  $args = array(
    'label' => __( 'Happy Clients', 'mawt' ),
    'description' => __( 'Happy Clients Description', 'mawt' ),
    'labels' => $labels,
    // las taxonomías que soportará
    //'taxonomies' => array( 'category', 'post_tag' ),
    // Todo lo que soporta este post type
        'supports' => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields' ),
        //hierarchical true se comporta como página, false como entrada
        'hierarchical' => false,
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'menu_position' => 5,
    //El icono que tendrá https://developer.wordpress.org/resource/dashicons
    'menu_icon' => 'dashicons-smiley',
    'show_in_admin_bar' => true,
    'show_in_nav_menus' => true,
    'can_export' => true,
    'has_archive' => true,
    'exclude_from_search' => false,
    'publicly_queryable' => true,
    'capability_type' => 'page',
  );

  register_post_type( 'a_happy_client', $args );
}

add_action( 'init', 'custom_happy_clients', 0 );

function custom_categoria_happy() {
  $labels = array(
    'name' => _x( 'Categorias', 'Categoria General Name', 'mawt' ),
    'singular_name' => _x( 'Categoria', 'Taxonomy Singular Name', 'mawt' ),
    'menu_name' => __( 'Categoria', 'mawt' ),
    'all_items' => __( 'All Items', 'mawt' ),
    'parent_item' => __( 'Parent Item', 'mawt' ),
    'parent_item_colon' => __( 'Parent Item:', 'mawt' ),
    'new_item_name' => __( 'New Item Name', 'mawt' ),
    'add_new_item' => __( 'Add New Item', 'mawt' ),
    'edit_item' => __( 'Edit Item', 'mawt' ),
    'update_item' => __( 'Update Item', 'mawt' ),
    'view_item' => __( 'View Item', 'mawt' ),
    'separate_items_with_commas' => __( 'Separate items with commas', 'mawt' ),
    'add_or_remove_items' => __( 'Add or remove items', 'mawt' ),
    'choose_from_most_used' => __( 'Choose from the most used', 'mawt' ),
    'popular_items' => __( 'Popular Items', 'mawt' ),
    'search_items' => __( 'Search Items', 'mawt' ),
    'not_found' => __( 'Not Found', 'mawt' ),
    'no_terms' => __( 'No items', 'mawt' ),
    'items_list' => __( 'Items list', 'mawt' ),
    'items_list_navigation' => __( 'Items list navigation', 'mawt' ),
  );

  $args = array(
    'labels' => $labels,
    //hierarchical true se comporta como categoría, false como etiqueta
    'hierarchical' => true,
    'public' => true,
    'show_ui' => true,
    'show_admin_column' => true,
    'show_in_nav_menus' => true,
    'show_tagcloud' => true,
  );
  register_taxonomy( 'a_categoria_happy', array( 'a_happy_client' ), $args );
}

add_action( 'init', 'custom_categoria_happy', 0 );



























    //post type avanzado

add_action('init', 'blog_post_types');

function blog_post_types() {
    //labels
    $labels = array(
        'name'                  => _x( 'Blogs', 'Post Type General Name', 'blog' ),
        'singular-name'         => _x( 'Blogs', 'Post Type Singular Name', 'blog' ),
        'menu_name'             => __( 'Blogs', 'blog' ),
        'parent_item_colon'     => __( 'Parent blog', 'blog' ),
        'all_items'             => __( 'All Blogs', 'blog' ),
        'view_item'             => __( 'View blog', 'blog' ),
        'add_new_item'          => __( 'Add New blog', 'blog' ),
        'add_new'               => __( 'Add New blog', 'blog' ),
        'edit_item'             => __( 'Edit blog', 'blog' ),
        'update_item'           => __( 'Update blog', 'blog' ),
        'search_items'          => __( 'Search RBlogsecipe', 'blog' ),
        'not_found'             => __( 'Not Blogs Found', 'blog' ),
        'not_found_in_trash'    => __( 'Not Found in Trash', 'blog' ),
        
    );
    
    //Another Customizations
    $args = array(
        'label'         => __( 'Blogs', 'blog'),
        'description'   => __( 'Blogs for gourmet Artistry', 'blog'),
        'labels'            => $labels,
        'supports'      => array( 'title', 'editor', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields'),
        'hierarchical' => false,
        'pubic'         => true,
        'show_ui'       => true,
        'has_archive' => true,
        'show_in_menus'=> true,
        'show_in_nav_menus'     => true,
        'show_in_admin_bar'     => true,
        'show_position'         => 5,
        'menu_icon'             => 'dashicons-admin-page',
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'capability_type'       => 'page',
        //insertar codigo para single-{$posttype}
        'publicly_queryable'    => true,

    );


    // register the post type
    register_post_type( 'blog', $args);
}












    //post type avanzado

add_action('init', 'portafolio_post_types');

function portafolio_post_types() {
    //labels
    $labels = array(
        'name'                  => _x( 'Portafolios', 'Post Type General Name', 'portafolio' ),
        'singular-name'         => _x( 'Portafolios', 'Post Type Singular Name', 'portafolio' ),
        'menu_name'             => __( 'Portafolios', 'portafolio' ),
        'parent_item_colon'     => __( 'Parent Portafolio', 'portafolio' ),
        'all_items'             => __( 'All Portafolios', 'portafolio' ),
        'view_item'             => __( 'View Portafolio', 'portafolio' ),
        'add_new_item'          => __( 'Add New Portafolio', 'portafolio' ),
        'add_new'               => __( 'Add New Portafolio', 'portafolio' ),
        'edit_item'             => __( 'Edit Portafolio', 'portafolio' ),
        'update_item'           => __( 'Update Portafolio', 'portafolio' ),
        'search_items'          => __( 'Search RPortafoliosecipe', 'portafolio' ),
        'not_found'             => __( 'Not Portafolios Found', 'portafolio' ),
        'not_found_in_trash' => __( 'Not Found in Trash', 'portafolio' ),
    );
    
    //Another Customizations
    $args = array(
        'label'         => __( 'Portafolios', 'portafolio'),
        'description'   => __( 'Portafolios for gourmet Artistry', 'portafolio'),
        'labels'            => $labels,
        'supports'      => array( 'title', 'editor', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields'),
        'hierarchical' => false,
        'pubic'         => true,
        'show_ui'       => true,
        'show_in_menus'=> true,
        'show_in_nav_menus'     => true,
        'show_in_admin_bar'     => true,
        'show_position'         => 5,
        'menu_icon'                 => 'dashicons-admin-page',
        'can_export'                => true,
        'has_archive'               => true,
        'exclude_from_search'   => false,
        'capability_type'               => 'page',

    );


    // register the post type
    register_post_type( 'portafolio', $args);
}



add_action('init', 'portafolio_type_taxonomy');

function portafolio_type_taxonomy(){
    $labels = array(
        'name'                  => _x( 'Portafolios', 'Post Type General Name' ),
        'singular-name'         => _x( 'Portafolios', 'Post Type Singular Name' ),
        'menu_name'             => __( 'Portafolios', 'portafolio' ),
        'parent_item_colon'     => __( 'Parent Portafolios' ),
        'all_items'             => __( 'All Portafolios' ),
        'update_item'           => __( 'Update Portafolio' ),
        //'view_item'           => __( 'View Portafolios', 'gourmet-artist' ),
        'add_new_item'          => __( 'Add New Portafolios' ),
        //'add_new'                 => __( 'Add New Portafolios', 'gourmet-artist' ),
        //'edit_item'               => __( 'Edit Portafolios', 'gourmet-artist' ),
        'update_item'           => __( 'Update Portafolios' ),
        'search_items'          => __( 'Search Portafolios' ),
        'new_item_name'     => __( 'New Portafolio' ),
        'edit_item'             => __( 'Edit Portafolio' ),
        //'not_found'               => __( 'Not Portafolios Found', 'gourmet-artist' ),
        //'not_found_in_trash' => __( 'Not Found in Trash', 'gourmet-artist' ),
    );

    $args = array(
        'hierarchical'      => true, 
        'labels'                    => $labels,
        'show_ui'               => true, //add the default UI to this taxonomy
        'show_admin_column' => true, //add the taxonomies to the wordpress admin
        'query_var'             => true,
        'rewrite'               => array( 'slug' => 'meal'),
    );
    register_taxonomy( 'portafolio_type', 'portafolio', $args );
}

