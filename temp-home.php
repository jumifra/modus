<?php
/*
 * Template name: Home
 */
get_header();
?>
	
	<?php part('home-slider'); ?>
	
	
	<p><?php get_taxonomies(); ?></p>
	<section class="homeServices">
		<div class="container">
			<div class="homeServices_top">
				<div class="homeServices_top_in">
					<h2 class="homeServices_top__title">Some of our top services</h2>
					<p class="homeServices_top__txt">Ut eleifend libero sed neque rhoncus consequat. Maecenas tincidunt, augue et rutrum condimentum, libero lectus mattis orci, ut commodo. </p>
				</div>
				<div class="homeServices_top__link">
					<a href="#" class="homeServices_top__link--int">VIEW MORE</a>
				</div>
			</div>
			<div class="homeServices_in">
				<article class="homeServices_content">
					<div class="homeServices_content__int">
						<i class="fas fa-thumbs-up homeServices_content__ico"></i>
						<h3 class="homeServices_content__title">SUSPENDISSE</h3>
						<p class="homeServices_content__txt">Ut eleifend libero sed neque rhoncus consequat. Maecenas tincidunt, augue et rutrum condimentum, libero lectus mattis orci, ut commodo.</p>
					</div>
					<a href="#" class="homeServices_content__link">read more</a>
				</article>
				<article class="homeServices_content">
					<div class="homeServices_content__int">
						<i class="fas fa-key homeServices_content__ico"></i>
						<h3 class="homeServices_content__title">MAECENAS</h3>
						<p class="homeServices_content__txt">Ut eleifend libero sed neque rhoncus consequat. Maecenas tincidunt, augue et rutrum condimentum, libero lectus mattis orci, ut commodo.</p>
					</div>
					<a href="#" class="homeServices_content__link">read more</a>
				</article>
				<article class="homeServices_content">
					<div class="homeServices_content__int">
						<i class="fas fa-flag homeServices_content__ico"></i>
						<h3 class="homeServices_content__title">ALIQUAM</h3>
						<p class="homeServices_content__txt">Ut eleifend libero sed neque rhoncus consequat. Maecenas tincidunt, augue et rutrum condimentum, libero lectus mattis orci, ut commodo.</p>
					</div>
					<a href="#" class="homeServices_content__link">read more</a>
				</article>
				<article class="homeServices_content">
					<div class="homeServices_content__int">
						<i class="fas fa-flask homeServices_content__ico"></i>
						<h3 class="homeServices_content__title">HABITASSE</h3>
						<p class="homeServices_content__txt">Ut eleifend libero sed neque rhoncus consequat. Maecenas tincidunt, augue et rutrum condimentum, libero lectus mattis orci, ut commodo.</p>
					</div>
					<a href="#" class="homeServices_content__link">read more</a>
				</article>
			</div>
		</div>
	</section>
	<section class="homeWhy">
		<div class="homeWhy-lineas"></div>
		<div class="container">
			<h2 class="homeWhy_title">WHY MODUS VERSUS?</h2>
			<p class="homeWhy_txt">Capacitance cascading integer reflective interface data development high bus cache dithering transponder.</p>
		</div>
		<div class="homeWhy--bg">
			<div class="homeWhy_content container">
				<div class="prueba">
					<div class="homeWhy_photo homeWhy_photo--lf">
						<div class="homeWhy_photo--iconos">
							<div id="homeWhy--pager--left" class="homeWhy_photo--iconos--circle">
							</div>
							<div class="homeWhy_photo--iconos--line">
								<i class="fas fa-minus"></i>
							</div>
						</div>
						<div class="homeWhy_photo--image cycle-slideshow"
							data-cycle-fx="scrollHorz"
	    					data-cycle-pause-on-hover="true"
	    					data-cycle-speed="200"
	    					data-cycle-pager="#homeWhy--pager--left"
	    					data-cycle-pager-template='<i class="fas fa-circle"></i>'
						>
							<img src="<?php echo get_template_directory_uri(); ?>/img/foto1.jpg" class="homeWhy_photo--image-img js-fit-image"/>
							<img src="<?php echo get_template_directory_uri(); ?>/img/foto2.jpg" class="homeWhy_photo--image-img js-fit-image"/>
							<img src="<?php echo get_template_directory_uri(); ?>/img/foto3.jpg" class="homeWhy_photo--image-img js-fit-image"/>
						</div>
					</div>
				</div>
				
				<div class="homeWhy_photo homeWhy_photo--center">
					<div class="homeWhy_photo--iconos">
						<div id="homeWhy--pager--center" class="homeWhy_photo--iconos--circle">
						</div>
						<div class="homeWhy_photo--iconos--line">
							<i class="fas fa-minus"></i>
						</div>
					</div>
					<div class="homeWhy_photo--image homeWhy_photo--image--center cycle-slideshow"
						data-cycle-fx="scrollHorz"
						data-cycle-pause-on-hover="true"
						data-cycle-speed="200"
						data-cycle-pager="#homeWhy--pager--center"
						data-cycle-pager-template='<i class="fas fa-circle"></i>'
					>
						<img src="<?php echo get_template_directory_uri(); ?>/img/foto1.jpg" class="homeWhy_photo--image-img js-fit-image"/>
						<img src="<?php echo get_template_directory_uri(); ?>/img/foto2.jpg" class="homeWhy_photo--image-img js-fit-image"/>
						<img src="<?php echo get_template_directory_uri(); ?>/img/foto3.jpg" class="homeWhy_photo--image-img js-fit-image"/>
					</div>
				</div>
				<div class="prueba">
					<div class="homeWhy_photo homeWhy_photo--rg">
						<div class="homeWhy_photo--iconos">
							<div id="homeWhy--pager--right" class="homeWhy_photo--iconos--circle">
							</div>
							<div class="homeWhy_photo--iconos--line">
								<i class="fas fa-minus"></i>
							</div>
						</div>
						<div class="homeWhy_photo--image cycle-slideshow"
							data-cycle-fx="scrollHorz"
	    					data-cycle-pause-on-hover="true"
	    					data-cycle-speed="200"
	    					data-cycle-pager="#homeWhy--pager--right"
	    					data-cycle-pager-template='<i class="fas fa-circle"></i>'
					>
							<img src="<?php echo get_template_directory_uri(); ?>/img/foto1.jpg" class="homeWhy_photo--image-img js-fit-image"/>
							<img src="<?php echo get_template_directory_uri(); ?>/img/foto2.jpg" class="homeWhy_photo--image-img js-fit-image"/>
							<img src="<?php echo get_template_directory_uri(); ?>/img/foto3.jpg" class="homeWhy_photo--image-img js-fit-image"/>
						</div>
					</div>
				</div>
				
			</div><!-- fin homeWhy_content -->
		</div>
	</section>
	<?php part('home-whychoose'); ?>
	<?php part('home-happyclients'); ?>
<?php get_footer(); ?>