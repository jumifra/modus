/*
 Agregar .js-fit-image a todas las imágnenes que se
 ajustarán a su contenedor.
 */

jQuery(function($){
    var $fit_image = $('.js-fit-image'),
        $window = $(window);

    function getCssObj( i_ratio, parent_width, parent_height, mode, x_anchor, y_anchor, min_width, min_height ) {
        var css_obj;

        css_obj = getRegularCssObject(i_ratio, parent_width, parent_height, mode, x_anchor, y_anchor, min_width, min_height);

        return css_obj;
    }

    function getRegularCssObject( i_ratio, parent_width, parent_height, mode, x_anchor, y_anchor, min_width, min_height ){

        var css_obj = {},
            h_ratio = parent_height / parent_width,
            i_height,
            i_width,
            correction,
            cover_factor;


        cover_factor = mode == 'cover' ? 1 : -1;

        if( cover_factor * i_ratio >= cover_factor * h_ratio ) {
            i_height = parent_width * i_ratio;

            css_obj.left = 0;
            css_obj.top = (( parent_height - i_height ) * y_anchor / parent_height * 100 ) + '%';
            css_obj.width = '113%';
            css_obj.height = 'auto';

            if( typeof min_width != 'undefined' && min_width > parent_width ){
                correction = ((min_width / parent_width) * 100 );
                css_obj.width = correction + '%';
                css_obj.left = ((100 - correction) * x_anchor) + '%';
            }

        } else {
            i_width = parent_height / i_ratio;

            css_obj.top = 0;
            css_obj.left = (( parent_width - i_width ) * x_anchor / parent_width * 100 ) + '%';
            css_obj.height = '130%';
            css_obj.width = 'auto';

            if( typeof min_height != 'undefined' && min_height > parent_height ){
                correction = ((min_height / parent_height) * 100 );
                css_obj.height = correction + '%';
                css_obj.top = ((100 - correction) * y_anchor ) + '%';
            }
        }

        return css_obj;
    }

    function resizeHandler() {

        $fit_image.recalculate();

    }

    function fitImagePrepare( $elements ) {

        $elements.css({
            opacity: 0 ,
            position : 'absolute'
        });

        $elements.each( function( i, el) {

            var $image          = $(el),
                $parent         = $image.parent(),
                image_ratio     = $image.data('ratio'),
                mode            = $image.data('mode') || 'cover',
                y_anchor        = $image.data('y-poi'),
                x_anchor        = $image.data('x-poi'),
                recalculate     = $image.data('recalculate') != 'false',

                min_width       = $image.data('min-width'),
                min_height      = $image.data('min-height');

            if (!image_ratio) {
                image_ratio = parseInt( $image.attr('height'),10 ) / parseInt( $image.attr('width'), 10);
                $image.data('ratio', image_ratio);
            }

            if(isNaN( image_ratio )){
                console.warn('Fit image, no se envió width o height para la imagen', $image.attr('src'));
            }


            function calculate(){


                var parent_height   = $parent.data('fi-height') || $parent.outerHeight(),
                    parent_width    = $parent.data('fi-width') || $parent.outerWidth(),
                    css_obj;

                // Min 0, max 1

                if( typeof x_anchor === 'undefined'){
                    x_anchor = 0.5;
                }

                if( typeof y_anchor === 'undefined'){
                    y_anchor = 0.5;
                }

                x_anchor = Math.max(0, Math.min(1, x_anchor));
                y_anchor = Math.max(0, Math.min(1, y_anchor));

                parent_height = parent_height == 'wh' ? $window.height() : parent_height;
                parent_width = parent_width == 'ww' ? $window.width() : parent_width;

                css_obj = getCssObj( image_ratio, parent_width, parent_height, mode, x_anchor, y_anchor, min_width, min_height );

                $image
                    .css( css_obj );
            }

            calculate();

            if(recalculate != '0'){
                $window.on('resize', calculate);
            }

            $image[0].recalculate = calculate;

        });

        $elements.recalculate = function(){
            $elements.each(function(i,el){
                el.recalculate();
            });
        };

        $elements.each( function( i, el){
            var $image = $(el),
                opacity = $image.data('opacity') || 1,
                img = new Image();

            img.onload = function(){
                $image.animate({opacity: opacity}, 400);
            };

            img.src = $image.attr('src');

        });
    }

    fitImagePrepare($fit_image);
    resizeHandler();


    function prepareContainer( $container ) {
        fitImagePrepare( $container.find('.js-fit-image') );
    }

    $(window).on('resize', resizeHandler );
    $(window).on('load', resizeHandler );

    window.fitImageContainer = prepareContainer;
    window.fitImagePrepare = fitImagePrepare;



});