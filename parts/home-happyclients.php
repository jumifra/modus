<?php 
			$wp_query = new WP_Query( array(
		      'post_type' => 'a_happy_client',
		      'posts_per_page' => 5,
		      'post_status' => 'publish',
		      'tax_query' => array(
		        array(
		          'taxonomy' => 'a_categoria', // Nombre de la taxonomía creada
		          'field' => 'slug', // Como pasaremos el parámetro, en este caso como el slug
		          'terms' => 'happy_clients', // Slug de la taxonomía
		        )
		      )
		    ));
		     // Loop WordPress
		     while ($wp_query->have_posts()) : $wp_query->the_post();
		       the_content();
		     endwhile;	
			?>


<div class="homeWhychoose--clients">
			<div class="container container-hidden">
				<div class="homeWhychoose--clients--top">
					<div class="homeWhychoose--clients--top--int">
						<?php if( have_rows('happy_clients') ): 

							while( have_rows('happy_clients') ): the_row(); 
								
								// vars
								$happyclientstitulo = get_sub_field('happy_clients_titulo');
								$happyclientsimage = get_sub_field('happy_clienst_image');
								
								?>
								
								<h3 class="homeWhychoose--clients--top--int--title"><?php echo $happyclientstitulo; ?></h3>

							<?php endwhile; ?>
							
						<?php endif; ?>
						<!-- <div class="homeWhychoose--clients--top--int--line">
							<div class="homeWhychoose--clients--top--int--line--int">
								
							</div>
						</div> -->
					</div>
					
					<div class="homeWhychoose--clients--top--controls">
					    <a href="#" id="prev"><i class="fas fa-chevron-left homeWhychoose--clients--top--controls--arrows"></i></a>
					    <a href="#" id="next"><i class="fas fa-chevron-right homeWhychoose--clients--top--controls--arrows"></i></a>
					</div>
				</div>
					<?php if( have_rows('happy_clients') ): 

						while( have_rows('happy_clients') ): the_row(); 
							
							// vars
							$happyclientstitulo = get_sub_field('happy_clients_titulo');
							$happyclientsimage = get_sub_field('happy_clienst_image');
							
							?>
							
							<?php if( have_rows('happy_clienst_image') ): ?>

								<div class="homeWhychoose--clients--slider cycle-slideshow" data-cycle-fx=carousel data-cycle-timeout=2000 data-cycle-carousel-visible=6 data-cycle-carousel-fluid=true data-cycle-slides=".homeWhychoose--clients--slider--int" data-cycle-next=#next data-cycle-prev=#prev data-cycle-pager=#pager>

									<?php while( have_rows('happy_clienst_image') ): the_row(); 

										// vars
										$happyclientsimg = get_sub_field('happy_clients_img');
										?>

										<div class="homeWhychoose--clients--slider--int"><img src="<?php echo $happyclientsimg; ?>" class="homeWhychoose--clients--slider--int--image"/></div>

									<?php endwhile; ?>

								</div>

							<?php endif; ?>


						<?php endwhile; ?>
						
					<?php endif; ?>
			    
				<!-- <div class="homeWhychoose--clients--slider cycle-slideshow" data-cycle-fx=carousel data-cycle-timeout=2000 data-cycle-carousel-visible=6 data-cycle-carousel-fluid=true data-cycle-slides=".homeWhychoose--clients--slider--int" data-cycle-next=#next data-cycle-prev=#prev data-cycle-pager=#pager>
					<div class="homeWhychoose--clients--slider--int"><img src="<?php echo get_template_directory_uri(); ?>/img/foto1.jpg" class="homeWhychoose--clients--slider--int--image"/></div>
				    <div class="homeWhychoose--clients--slider--int"><img src="<?php echo get_template_directory_uri(); ?>/img/foto1.jpg" class="homeWhychoose--clients--slider--int--image"/></div>
				    <div class="homeWhychoose--clients--slider--int"><img src="<?php echo get_template_directory_uri(); ?>/img/foto1.jpg" class="homeWhychoose--clients--slider--int--image"/></div>
				    <div class="homeWhychoose--clients--slider--int"><img src="<?php echo get_template_directory_uri(); ?>/img/foto1.jpg" class="homeWhychoose--clients--slider--int--image"/></div>
				    <div class="homeWhychoose--clients--slider--int"><img src="<?php echo get_template_directory_uri(); ?>/img/foto1.jpg" class="homeWhychoose--clients--slider--int--image"/></div>
				    <div class="homeWhychoose--clients--slider--int"><img src="<?php echo get_template_directory_uri(); ?>/img/foto1.jpg" class="homeWhychoose--clients--slider--int--image"/></div>
				    <div class="homeWhychoose--clients--slider--int"><img src="<?php echo get_template_directory_uri(); ?>/img/foto1.jpg" class="homeWhychoose--clients--slider--int--image"/></div>
				    <div class="homeWhychoose--clients--slider--int"><img src="<?php echo get_template_directory_uri(); ?>/img/foto1.jpg" class="homeWhychoose--clients--slider--int--image"/></div>
				    <div class="homeWhychoose--clients--slider--int"><img src="<?php echo get_template_directory_uri(); ?>/img/foto1.jpg" class="homeWhychoose--clients--slider--int--image"/></div>
				    <div class="homeWhychoose--clients--slider--int"><img src="<?php echo get_template_directory_uri(); ?>/img/foto1.jpg" class="homeWhychoose--clients--slider--int--image"/></div>
				    <div class="homeWhychoose--clients--slider--int"><img src="<?php echo get_template_directory_uri(); ?>/img/foto1.jpg" class="homeWhychoose--clients--slider--int--image"/></div>
			    </div>  -->
			    <!-- <div class="center">
				    <a href="#" id="prev">&lt;&lt; Prev </a>
				    <a href="#" id="next"> Next &gt;&gt; </a>
				</div> -->
				                                    
				<!-- <div class="cycle-pager" id="pager"></div>
				<script>
				     $.fn.cycle.defaults.autoSelector = '.slideshow';
				</script> -->
			    
					
			</div>
		</div>