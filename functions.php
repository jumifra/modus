<?php

/**
*My Awesome Wordpress Theme
*
*@package Wordpress
*@subpackage modus
*@since 1.0.0
*@version 1.0.0
*/


require_once 'common.php';

//scripts css y js
if(!function_exists('modus_scripts')):
	function modus_scripts(){
		wp_register_style('google-fonts','https://use.typekit.net/rqe3amd.css',array(), '1.0.0','all');
		//wp_register_style('fontawesomecss',get_template_directory_uri().'/src/css/all.css','1.0.0');
		wp_register_style('style',get_stylesheet_uri(),array('google-fonts'), '1.0.0','all');

		wp_enqueue_style('google-fonts');
		wp_enqueue_style('style');

		wp_register_script('jqueryslider',get_template_directory_uri().'/src/js/jquery-3.4.1.min.js','1.0.0');
		wp_register_script('slick',get_template_directory_uri().'/src/js/slick.js', array('jquery'),'1.0.0',true);
		wp_register_script('scripts',get_template_directory_uri().'/src/js/script.js', array('jquery'),'1.0.0',true);
		wp_register_script('cycle',get_template_directory_uri().'/src/js/jquery.cycle2.js', array('jquery'),'1.0.0',true);
		wp_register_script('cyclecarousel',get_template_directory_uri().'/src/js/jquery.cycle2.carousel.js', array('jquery'),'1.0.0',true);
		wp_register_script('fitimage',get_template_directory_uri().'/src/js/fit-image.js', array('jquery'),'1.0.0',true);
		wp_register_script('serviciosCarousel',get_template_directory_uri().'/src/js/serviciosCarousel.js', array('jquery'),'1.0.0',true);
		//wp_register_script('owl',get_template_directory_uri().'/src/js/owl.carousel.min.js', array('jquery'),'1.0.0',true);
		//wp_register_script('fontawesome',get_template_directory_uri().'/src/js/all.js','1.0.0',true);
		

		wp_enqueue_script('jquery');
		wp_enqueue_script('jqueryslider');
		wp_enqueue_script('slick');
		wp_enqueue_script('cycle');
		wp_enqueue_script('cyclecarousel');
		wp_enqueue_script('fitimage');
		wp_enqueue_script('serviciosCarousel');
		//wp_enqueue_script('owl');
		
		wp_enqueue_script('scripts');
		//wp_enqueue_script('fontawesome');
		//wp_enqueue_script('fontawesomecss');
		
		
	}
endif;

add_action('wp_enqueue_scripts','modus_scripts');

//registrar menu y menu redes sociales
if(!function_exists('registrar_menu')):
	function registrar_menu(){
		register_nav_menus(array(
			'main_menu'=>__('Menu Principal', 'modus'),
			'social_menu'=>__('Menu Redes Sociales', 'modus')
		));
	}

endif;

add_action('init','registrar_menu');



//Soporte HTML5 de buscador

add_theme_support('html5',array(
	'comment-list',
	'commet-form',
	'search-form',
	'gallery',
	'caption'
));


 




function part( $name, $suffix = ''){
    get_template_part('parts/' . $name, $suffix);
}





	
// Insertar Breadcrumb    
function the_breadcrumbb() {
 if (!is_home()) {
	 echo '<p class="removed_link" title="';
	 echo get_option('home');
	         echo '">';
	         echo "Home";
	 //bloginfo('name');
	 echo "</p>&nbsp;/&nbsp;";
	 if (is_category() || is_single()) {
	 the_category('title_li=');
	 if (is_single()) {
	 echo " » ";
	 echo "<p>";
	 the_title();
	 echo "</p>";
	 }
	 } elseif (is_page()) {
	 echo "<p>";
	 echo the_title();
	 echo "</p>";
	 }
 }
}    
// fin breadcrumb


function the_breadcrumb() {

    $sep = ' &nbsp;/&nbsp;&nbsp; ';

    if (!is_front_page()) {

        echo '<div class="aboutUs--top--breadcrumbs">';
        echo '<a href="';
        echo get_option('home');
        echo '">';
        echo 'Home';
        //bloginfo('name');
        echo '</a>' . $sep;

        echo '<p>';

        if (is_category() || is_single() ){
            the_category('title_li=');
        } elseif (is_archive() || is_single()){
            if ( is_day() ) {
                printf( __( '%s', 'text_domain' ), get_the_date() );
            } elseif ( is_month() ) {
                printf( __( '%s', 'text_domain' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'text_domain' ) ) );
            } elseif ( is_year() ) {
                printf( __( '%s', 'text_domain' ), get_the_date( _x( 'Y', 'yearly archives date format', 'text_domain' ) ) );
            } else {
                _e( 'Blog Archives', 'text_domain' );
            }
        }

        if (is_single()) {
            echo $sep;
            the_title();
        }

        if (is_page()) {
            echo the_title();
        }

        if (is_home()){
            global $post;
            $page_for_posts_id = get_option('page_for_posts');
            if ( $page_for_posts_id ) { 
                $post = get_page($page_for_posts_id);
                setup_postdata($post);
                the_title();
                rewind_posts();
            }
        }
        echo '</p>';

        echo '</div>';
    }
}


//PicolSuperior::setPageParentForPostType( PostTypes::NAME_NOTICIAS, 'blog');