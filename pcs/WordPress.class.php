<?php

class WordPress{

    const POST_FORMAT_ASIDE = 'aside';
    const POST_FORMAT_GALLERY = 'gallery';
    const POST_FORMAT_LINK = 'link';
    const POST_FORMAT_QUOTE = 'quote';
    const POST_FORMAT_STATUS = 'status';
    const POST_FORMAT_VIDEO = 'video';
    const POST_FORMAT_AUDIO = 'audio';
    const POST_FORMAT_CHAT = 'chat';

    const POST_TYPE_SUPPORT_TITLE = 'title';
    const POST_TYPE_SUPPORT_EDITOR = 'editor';
    const POST_TYPE_SUPPORT_AUTHOR = 'author';
    const POST_TYPE_SUPPORT_THUMBNAIL = 'thumbnail';
    const POST_TYPE_SUPPORT_EXCERPT = 'excerpt';
    const POST_TYPE_SUPPORT_TRACKBACKS = 'trackbacks';
    const POST_TYPE_SUPPORT_CUSTOM_FIELDS = 'custom-fields';
    const POST_TYPE_SUPPORT_COMMENTS = 'comments';
    const POST_TYPE_SUPPORT_REVISIONS = 'revisions';
    const POST_TYPE_SUPPORT_PAGE_ATTRIBUTES = 'page-attributes';
    const POST_TYPE_SUPPORT_POST_FORMATS = 'post-formats';

    const THEME_SUPPORT_THUMBNAILS = 'post-thumbnails';
    const THEME_SUPPORT_FORMATS = 'post-formats';
    const THEME_SUPPORT_CUSTOM_BACKGROUND = 'custom-background';

    const POST_STATUS_PUBLISHED = 'publish';
    const POST_STATUS_FUTURE = 'future';
    const POST_STATUS_DRAFT = 'draft';
    const POST_STATUS_PENDING = 'pending';
    const POST_STATUS_PRIVATE = 'private';
    const POST_STATUS_TRASH = 'trash';
    const POST_STATUS_AUTO_DRAFT = 'auto-draft';
    const POST_STATUS_INHERIT = 'inherit';

}




