<section class="homeSlider">
	<div class="homeSlider-content">
		<div class="homeSlider-int">
			<img src="<?php echo get_template_directory_uri(); ?>/img/foto1.jpg" class="homeSlider-int-img"/>
			<div class="homeSlider-int-content">
				<h2 class="homeSlider-int-content-vert-titulo">VESTIBULUM</h2>
				<p class="homeSlider-int-content-vert-txt">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat repudiandae, consequuntur natus rerum, veniam officia!</p>
			</div>
		</div>
		<div class="homeSlider-int">
			<img src="<?php echo get_template_directory_uri(); ?>/img/foto1.jpg" class="homeSlider-int-img"/>
			<div class="homeSlider-int-content">
				<h2 class="homeSlider-int-content-vert-titulo">VESTIBULUM</h2>
				<p class="homeSlider-int-content-vert-txt">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat repudiandae, consequuntur natus rerum, veniam officia!</p>
			</div>
		</div>
		<div class="homeSlider-int">
			<img src="<?php echo get_template_directory_uri(); ?>/img/foto1.jpg" class="homeSlider-int-img"/>
			<div class="homeSlider-int-content">
				<h2 class="homeSlider-int-content-vert-titulo">VESTIBULUM</h2>
				<p class="homeSlider-int-content-vert-txt">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat repudiandae, consequuntur natus rerum, veniam officia!</p>
			</div>
		</div>
	</div> 
	<script>
		$(document).ready(function(){
		  $('.homeSlider-content').slick({
		    dots: true,
		    arrows: true,
		    autoplay: true,
		    autoplaySpeed: 1000
		  });
		});
	</script>
</section>