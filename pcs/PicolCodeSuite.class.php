<?php
/**
 * Created by PhpStorm.
 * User: rodrigomurillo
 * Date: 2/19/15
 * Time: 4:20 PM
 */

class PicolCodeSuite{

    protected $repositories = array();

    protected static $instance;

    protected static function getInstance(){
        if(!isset(self::$instance)){
            self::$instance = new PicolCodeSuite();
        }

        return self::$instance;
    }

    static function _start(){

        $pcs = self::getInstance();

        $pcs->loadTextDomain();
        $pcs->registerAutoloadFunctions();
        add_action('after_setup_theme', array(&$pcs, 'afterThemeSetup'));

    }

    static function themeSetup( $__file__, $callable ) {
        self::getInstance()->addRepository( $__file__ );

        if( is_callable($callable)){
            add_action('picol_init', $callable);
        }
    }

    static function pluginSetup( $__file__, $callable ){
        self::getInstance()->addRepository( $__file__ );

        if(is_callable($callable)){
            add_action('picol_loaded', $callable);
        }
    }

    /*
     * Instance functinos
     */

    protected function __construct(){
    }

    function autoload($class_name){
        static $theme_root;
        if(!isset($theme_root)){
            $theme_root = get_template_directory();
        }

        $filename =  dirname(__FILE__).'/includes/i-' . $class_name . '.php';

        if( file_exists( $filename )) {
            include $filename;
        } else {

            $found = FALSE;

            foreach($this->repositories as $repo){
                if($found = $this->includeClassFile($class_name, $repo))
                    break;
            }

            if(!$found){
                $this->includeClassFile($class_name, $theme_root);
            }

        }

    }

    function includeClassFile($class_name, $root_dir){

        $filename = FALSE;

        $tree = array(
            'modules'  => $root_dir . '/app/modules/m-' . $class_name . '.php',
            'classes'  => $root_dir . '/app/classes/c-' . $class_name . '.php'
        );

        foreach($tree as $type => $file) {

            if (file_exists($file)) {
                $filename = $file;
                break;
            }

        }

        // Maybe it's a post object

        if( $is_post_type = preg_match('/(?<!Picol)Post$/', $class_name) ){
            $filename = $root_dir . '/app/post-objects/p-' . $class_name . '.php';
        }

        if( $filename && file_exists( $filename )){
            include $filename;
            return TRUE;
        }

        return FALSE;
    }


    function loadTextDomain() {
        load_plugin_textdomain( 'pcs', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
    }



    protected function registerAutoloadFunctions()
    {
      spl_autoload_register(array(&$this, 'autoload'));
      require_once(dirname(__FILE__) . '/vendor/autoload.php');
    }

    function afterThemeSetup(){
        $this->startAllModules();
        do_action('picol_init');
    }

    function addRepository($__file__){
        $repo_path = dirname($__file__);
        $this->repositories[] = $repo_path;
    }

    function startAllModules(){
        if(!empty($this->repositories)){

            $prefix_length = strlen('m-');
            $suffix_length = strlen('.php');

            foreach($this->repositories as $repo){
                $modules = glob( $repo .'/app/modules/*.php');
                foreach($modules as $module_file ){
                    $basename = basename($module_file);
                    $class_name_length = strlen($basename) - $prefix_length - $suffix_length;
                    $class_name = substr( $basename, $prefix_length, $class_name_length );
                    $callable = array($class_name, 'init');

                    if(is_callable($callable)){
                        call_user_func($callable);
                    }
                }
            }
        }
    }

}