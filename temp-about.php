<?php
/*
 * Template name: About Us
 */
get_header();
?>


<div class="aboutUs">
	<div class="aboutUs--top">
		<div class="container">
				<p class="aboutUs--top--title"><?php echo get_the_title(); ?> </p>
				<!-- Breadcrumb -->
					<?php the_breadcrumb(); ?>
				<!-- Fin Breadcrumb -->
			
		</div>
	</div>
	<div class="aboutUs--int">
		<div class="container">
			<h3 class="aboutUs--int--title"><?php the_field('about_title_header') ?></h3>
			<p class="aboutUs--int--txt"><?php the_field('about_texto_cabecera') ?></p>
		</div>
	</div>
</div>


<?php part('home-whychoose'); ?>


<section class="aboutTeam">
	<div class="aboutTeam-lineas"></div>
	<div class="container">
		<h3 class="aboutTeam-title">OUR TEAM</h3>
		<div class="aboutTeam--grid">
			<div class="aboutTeam-item">
				<div class="aboutTeam-item--focus">
					<div class="aboutTeam-item--focus--int">
						<div class="aboutTeam-item--focus--int--image">
							<img src="<?php echo get_template_directory_uri(); ?>/img/person1.jpg" class="aboutTeam-item--focus--int--image--img"/>
						</div>
						<div class="aboutTeam-item--focus--int--info">
							<p class="aboutTeam-item--focus--int--info--txt">Curabitur quis nisl in leo euismod venenatis eu in diam. Etiam auctor diam pellentesque lectus vehicula mattis. Nulla ac massa at dolor condimentum</p>
							<ul class="aboutTeam-item--focus--int--info--socials">
								<li class="aboutTeam-item--focus--int--info--socials--social"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
								<li class="aboutTeam-item--focus--int--info--socials--social"><a href="#"><i class="fab fa-twitter"></i></a></li>
								<li class="aboutTeam-item--focus--int--info--socials--social"><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
								<li class="aboutTeam-item--focus--int--info--socials--social"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
							</ul>
						</div>
					</div>
					<div class="aboutTeam-item--focus--person">
						<a href="#" class="aboutTeam-item--focus--person--link">Jhon DOE</a>
					</div>
				</div>
			</div>
			<div class="aboutTeam-item">
				<div class="aboutTeam-item--focus">
					<div class="aboutTeam-item--focus--int">
						<div class="aboutTeam-item--focus--int--image">
							<img src="<?php echo get_template_directory_uri(); ?>/img/person3.jpg" class="aboutTeam-item--focus--int--image--img"/>
						</div>
						<div class="aboutTeam-item--focus--int--info">
							<p class="aboutTeam-item--focus--int--info--txt">Curabitur quis nisl in leo euismod venenatis eu in diam. Etiam auctor diam pellentesque lectus vehicula mattis. Nulla ac massa at dolor condimentum</p>
							<ul class="aboutTeam-item--focus--int--info--socials">
								<li class="aboutTeam-item--focus--int--info--socials--social"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
								<li class="aboutTeam-item--focus--int--info--socials--social"><a href="#"><i class="fab fa-twitter"></i></a></li>
								<li class="aboutTeam-item--focus--int--info--socials--social"><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
								<li class="aboutTeam-item--focus--int--info--socials--social"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
							</ul>
						</div>
					</div>
					<div class="aboutTeam-item--focus--person">
						<a href="#" class="aboutTeam-item--focus--person--link">Jhon DOE</a>
					</div>
				</div>
			</div>
			<div class="aboutTeam-item">
				<div class="aboutTeam-item--focus">
					<div class="aboutTeam-item--focus--int">
						<div class="aboutTeam-item--focus--int--image">
							<img src="<?php echo get_template_directory_uri(); ?>/img/person4.jpg" class="aboutTeam-item--focus--int--image--img"/>
						</div>
						<div class="aboutTeam-item--focus--int--info">
							<p class="aboutTeam-item--focus--int--info--txt">Curabitur quis nisl in leo euismod venenatis eu in diam. Etiam auctor diam pellentesque lectus vehicula mattis. Nulla ac massa at dolor condimentum</p>
							<ul class="aboutTeam-item--focus--int--info--socials">
								<li class="aboutTeam-item--focus--int--info--socials--social"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
								<li class="aboutTeam-item--focus--int--info--socials--social"><a href="#"><i class="fab fa-twitter"></i></a></li>
								<li class="aboutTeam-item--focus--int--info--socials--social"><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
								<li class="aboutTeam-item--focus--int--info--socials--social"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
							</ul>
						</div>
					</div>
					<div class="aboutTeam-item--focus--person">
						<a href="#" class="aboutTeam-item--focus--person--link">Jhon DOE</a>
					</div>
				</div>
			</div>
			<div class="aboutTeam-item">
				<div class="aboutTeam-item--focus">
					<div class="aboutTeam-item--focus--int">
						<div class="aboutTeam-item--focus--int--image">
							<img src="<?php echo get_template_directory_uri(); ?>/img/person5.jpg" class="aboutTeam-item--focus--int--image--img"/>
						</div>
						<div class="aboutTeam-item--focus--int--info">
							<p class="aboutTeam-item--focus--int--info--txt">Curabitur quis nisl in leo euismod venenatis eu in diam. Etiam auctor diam pellentesque lectus vehicula mattis. Nulla ac massa at dolor condimentum</p>
							<ul class="aboutTeam-item--focus--int--info--socials">
								<li class="aboutTeam-item--focus--int--info--socials--social"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
								<li class="aboutTeam-item--focus--int--info--socials--social"><a href="#"><i class="fab fa-twitter"></i></a></li>
								<li class="aboutTeam-item--focus--int--info--socials--social"><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
								<li class="aboutTeam-item--focus--int--info--socials--social"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
							</ul>
						</div>
					</div>
					<div class="aboutTeam-item--focus--person">
						<a href="#" class="aboutTeam-item--focus--person--link">Jhon DOE</a>
					</div>
				</div>
			</div>
			
		</div>
		
	</div>
</section>
<div class="bestSolution">
	<div class="container">
		<h3 class="bestSolution--title">Best Solution is the simplest IDEA!</h3>
		<p class="bestSolution--txt">Capacitance cascading integer reflective interface data development high bus cache dithering transponder. Curabitur vitae velit in neque dictum blandit. Proin in iaculis neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. </p>
	</div>
</div>
<?php part('home-happyclients'); ?>
<?php get_footer(); ?>


